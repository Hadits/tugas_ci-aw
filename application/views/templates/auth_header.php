<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>
    <?= $title;
    ?></title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url('') ?>assets/css/all.min.css" rel="stylesheet" type="text/css">

  <link href="<?= base_url('assets/') ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="<?= base_url('assets/') ?>css/sb-admin.css" rel="stylesheet">
  <link href="<?= base_url('assets/') ?>css/sb-admin.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>css/sb-admin-2.min.css" rel="stylesheet">
  <style>
    #snow {
      background-color: transparent;
      background-image: url(http://wp.agens.no/smakebit/wp-content/themes/smakebiten/img/snow1.png), url(http://wp.agens.no/smakebit/wp-content/themes/smakebiten/img/snow2.png);
      -webkit-animation: snow 20s linear infinite;
      -moz-animation: snow 20s linear infinite;
      -ms-animation: snow 20s linear infinite;
      animation: snow 20s linear infinite;
      z-index: 999;
      right: 0;
      top: 0;
      left: 0;
      bottom: 0;
      margin-top: 0;
      pointer-events: none;
      position: absolute;
    }

    /*Keyframes*/

    @keyframes snow {
      0% {
        background-position: 0px 0px, 0px 0px, 0px 0px
      }

      100% {
        background-position: 500px 1000px, 400px 400px, 300px 300px
      }
    }

    @-moz-keyframes snow {
      0% {
        background-position: 0px 0px, 0px 0px, 0px 0px
      }

      100% {
        background-position: 500px 1000px, 400px 400px, 300px 300px
      }
    }

    @-webkit-keyframes snow {
      0% {
        background-position: 0px 0px, 0px 0px, 0px 0px
      }

      100% {
        background-position: 500px 1000px, 400px 400px, 300px 300px;
      }
    }

    @-ms-keyframes snow {
      0% {
        background-position: 0px 0px, 0px 0px, 0px 0px
      }

      100% {
        background-position: 500px 1000px, 400px 400px, 300px 300px
      }
    }
  </style>
</head>


<body style="background: #a3d5d3">
  <section id="snow"></section>