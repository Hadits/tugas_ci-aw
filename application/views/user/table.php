<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title> Halaman Dashboard | AWP </title>
  <!-- Custom fonts for this template -->
  <link href="<?= base_url('assets/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?= base_url('assets/') ?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url('assets/'); ?>css/sb-admin.css" rel="stylesheet">
  <script src="<?= base_url('assets/'); ?>vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url('assets/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url('assets/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url('assets/'); ?>js/sb-admin.min.js"></script>
</head>

<body id="page-top">
  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="nav-link" href="<?= base_url('ctrl/logout') ?>">
      <i class="fas fa-fw fa-sign-out-alt" style="color:aqua"></i>
      <span style="color:aquamarine">Logout</span></a>

    <!-- <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="">
      <i class="fas fa-bars"></i>
    </button> -->

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-4 my-2 my-md-0">

    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $log_in['name']; ?></span>
          <img class="img-profile " alt=""></! </a> <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">

          <a class="dropdown-item " href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
          </div>
      </li>
    </ul>

  </nav>
  <div class="table-responsive">

    <table class="table table-bordered table-striped">

      <tr>

        <td><b>IP Address</b></td>

        <td><?php echo $ip_address; ?></td>

      </tr>

      <tr>

        <td><b>Operating System</b></td>

        <td><?php echo $os; ?></td>

      </tr>
      <tr>
        <td><b>Browser Details</b></td>
        <td><?php echo $browser . '' . $browser_version; ?></td>
      </tr>
    </table>
  </div>
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Begin Page Content -->
    <div class="container-fluid">
      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Data User</h6>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <?= $this->session->flashdata('message'); ?>
            <button id="btnAdd" class="btn btn-success">Add Table</button>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Is Active</th>
                  <th>QR Code</th>
                </tr>
              </thead>
              <tbody id="showData">
                <!-- 								<?php
                                      foreach ($tampil->result() as $row) { ?>
									            <tr>
										            <td class="border"><?php echo $row->name; ?></td>
										            <td class="border"><?php echo $row->email; ?></td>
										            <td class="border"><?php echo $row->password; ?></td>
										            <td class="border"><?php echo $row->is_active; ?></td>
										            <td><img src="<?= site_url('ctrl/QRcode/' . $row->email); ?>"></td>
									            </tr>
								<?php }
                ?> -->
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->

  </div>
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="" id="myForm" class="form-horizontal" method="post">
            <input type="hidden" name="detectId" values="0">
            <div class="form-group">
              <label for="name" class="label-control col-md-4">Username</label>
              <div class="col-md-12">
                <input type="text" class="form-control" name="name">
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="label-control col-md-4">Email</label>
              <div class="col-md-12">
                <input type="text" class="form-control" name="email">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="label-control col-md-4">Password</label>
              <div class="col-md-12">
                <input type="password" class="form-control" name="password">
              </div>
            </div>
            <div class="form-group">
              <label for="retype-password" class="label-control col-md-4">Retype-Password</label>
              <div class="col-md-12">
                <input type="password" class="form-control" name="retypePassword">
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" id="btnSave" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Confirm Delete</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Are you sure to delete this data?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" id="btnDelete" class="btn btn-danger">Delete</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End of Main Content -->
  <!-- Footer -->
  <footer class="sticky-footer bg-white">
    <div class="container my-auto">
      <div class="copyright text-center my-auto">
        <span>Copyright &copy; <?= $log_in['name'] ?></span>
      </div>
    </div>
  </footer>
  <!-- End of Footer -->
  </div>
  <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <script type="text/javascript">
    $(function() {
      //Show All Data
      showAllData();
      //Add New Menu
      $('#btnAdd').click(function() {
        $('#myModal').modal('show');
        $('#myModal').find('.modal-title').text('Add New Data');
        $('#myForm').attr('action', '<?= base_url() ?>ctrl/addNew');
      });

      $('#btnSave').click(function() {
        var url = $('#myForm').attr('action');
        var data = $('#myForm').serialize();
        //Validate Form
        var name = $('input[name=name]');
        var email = $('input[name=email]');
        var password = $('input[name=password]');
        var retypePassword = $('input[name=retypePassword]');
        var result = '';
        if (name.val() == '') {
          name.parent().parent().addClass('has-error');
        } else {
          name.parent().parent().removeClass('has-error');
          result += '1';
        }
        if (email.val() == '') {
          email.parent().parent().addClass('has-error');
        } else {
          email.parent().parent().removeClass('has-error');
          result += '2';
        }
        if (password.val() == '') {
          password.parent().parent().addClass('has-error');
        } else {
          password.parent().parent().removeClass('has-error');
          result += '3';
        }
        if (retypePassword.val() == '') {
          retypePassword.parent().parent().addClass('has-error');
        } else {
          retypePassword.parent().parent().removeClass('has-error');
          result += '4';
        }

        if (result == '1234') {
          $.ajax({
            type: 'ajax',
            method: 'post',
            url: url,
            data: data,
            async: true,
            dataType: 'json',
            success: function(response) {
              if (response.success) {
                $('#myModal').modal('hide');
                $('#myForm')[0].reset();
                if (response.type == 'add') {
                  var type = "Added";
                } else if (response.type == 'update') {
                  var type = "Updated";
                }
                $('#success').html('Data ' + type + ' Successfully!').fadeIn().delay(4000).fadeOut('slow');
                showAllData();
              } else {
                alert('Error');
              }
            },
            error: function() {
              alert('Could Not Add Data');
            }
          });
        }
      });

      //Function Edit Data
      $('#showData').on('click', '.item-edit', function() {
        var id = $(this).attr('data');
        $('#myModal').modal('show');
        $('#myModal').find('.modal-title').text('Edit Data');
        $('#myForm').attr('action', '<?= base_url() ?>ctrl/updateData');

        $.ajax({
          type: 'ajax',
          method: 'get',
          url: '<?= base_url() ?>ctrl/editData',
          data: {
            id: id
          },
          async: true,
          dataType: 'json',
          success: function(data) {
            $('input[name=name').val(data.name);
            $('input[name=email]').val(data.email);
            $('input[name=password]').val(data.password);
            $('input[name=retypePassword]').val(data.password);
            $('input[name=detectId]').val(data.id);
          },
          error: function() {
            alert('Could not Edit Data');
          }
        });
      });

      //Function Delete
      $('#showData').on('click', '.item-delete', function() {
        var id = $(this).attr('data');
        $('#deleteModal').modal('show');

        //Prevent Previous Handler - Unbind
        $('#btnDelete').unbind().click(function() {
          $.ajax({
            type: 'ajax',
            method: 'get',
            async: true,
            url: '<?= base_url() ?>ctrl/deleteData',
            data: {
              id: id
            },
            dataType: 'json',
            success: function(response) {
              if (response.success) {
                $('#deleteModal').modal('hide');
                $('.alert-success').html('Data deleted successfully').fadeIn().delay(4000).fadeOut('slow');
                showAllData();
              } else {
                alert('Error');
              }
            },
            error: function() {
              alert('Error Deleting');
            }
          });
        });
      });



      //Function Read Data
      function showAllData() {
        $.ajax({
          type: 'ajax',
          url: '<?= base_url() ?>ctrl/showAllData',
          async: true,
          dataType: 'json',
          success: function(data) {
            var html = '';
            var i;
            for (i = 0; i < data.length; i++) {
              html += '<tr>' +
                '<td>' + data[i].id + '</td>' +
                '<td>' + '<img src="<?= base_url() ?>ctrl/QRcode/' + data[i].email + '">' + '</td>' +
                '<td>' + data[i].name + '</td>' +
                '<td>' + data[i].email + '</td>' +
                '<td>' +
                '<a href="javascript:;" class="btn btn-info btn-block item-edit" data="' + data[i].id + '">Edit</a> ' +
                '<a href="javascript:;" class="btn btn-danger btn-block item-delete" data="' + data[i].id + '">Delete</a>' +
                '</td>' +
                '</tr>';

            }
            $('#showData').html(html);
          },
          error: function() {
            alert('Could not get data from database');
          }
        });
      }
    });
  </script>
  <!-- Bootstrap core JavaScript-->

</body>

</html>