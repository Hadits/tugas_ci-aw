<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ctrl extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('Ciqrcode');
		$this->load->model('user_model');
		$this->load->library('user_agent');
	}

	public function index()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');


		if ($this->form_validation->run() == false) {
			$dat['title'] = 'Login';
			$this->load->view('templates/auth_header', $dat);
			$this->load->view('auth/login');
			$this->load->view('templates/auth_footer');
		} else {
			$this->_login();
		}
	}
	private function _login()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$user = $this->db->get_where('log_in', ['email' => $email])->row_array();

		if ($user) {
			if ($user['is_active'] == 1) {
				if (password_verify($password, $user['password'])) {
					$data = [
						'email' => $user['email'],
					];
					$this->session->set_userdata($data);
					redirect('user/table');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">wrong password</div>');
					redirect('ctrl');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email is not registered!</div>');
				redirect('ctrl');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email is not registered!</div>');
			redirect('ctrl');
		}
	}

	public function QRcode($kodenya = '123')
	{

		QRcode::png(
			$kodenya,
			$outfile = false,
			$level = QR_ECLEVEL_H,
			$size = 5,
			$margin = 2
		);
	}


	public function register()
	{

		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[log_in.email]', [
			'is_unique' => 'This email has already used!'
		]);
		$this->form_validation->set_rules(
			'password1',
			'Password',
			'required|trim|min_length[4]|matches[password2]',
			[
				'matches' => 'password doesnt match', 'min_length' => 'password too short'
			]
		);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Register';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/register');
			$this->load->view('templates/auth_footer');
		} else {
			$form_response = $this->input->post('g-recaptcha-response');
			$url = "https://www.google.com/recaptcha/api/siteverify";
			$sitekey = "6Le_JqoUAAAAAKuAd4e8ba7aOVTY1VUzOhDfWf-m";
			$response = file_get_contents($url . "?secret=" . $sitekey . "&response=" . $form_response . "&remoteip=" . $_SERVER["REMOTE_ADDR"]);
			$data = json_decode($response);
			if (isset($data->success) && $data->success == "true") {

				//get user inputs
				$name = $this->input->post('name');
				$email = $this->input->post('email');

				//generate simple random code
				$set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$code = substr(str_shuffle($set), 0, 10);
				$data = [
					'name' => htmlspecialchars($this->input->post('name', true)),
					'email' => htmlspecialchars($this->input->post('email', true)),
					'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
					'is_active'	=> 0,
					// 'data_created' => 'NOW()',
					'code' => $code
					// 'image' => 'anjing.jpg'
				];

				// //insert user to users table and get id
				// $user['email'] = $email;
				// $user['password'] = $password;
				// $user['code'] = $code;
				// $user['is_active'] = false;
				$id = $this->user_model->insert($data);
				$encodeE = base64_encode($email);
				// Load PHPMailer library
				$this->load->library('phpmailer_lib');

				// PHPMailer object
				$mail = $this->phpmailer_lib->load();

				// SMTP configuration
				$mail->isSMTP();
				$mail->Host     = 'smtp.gmail.com';
				$mail->SMTPAuth = true;
				$mail->Username = 'hadizt339@gmail.com';
				$mail->Password = 'Hadits123';
				$mail->SMTPSecure = 'tls';
				$mail->Port     = 587;

				$mail->setFrom('hadizt339@gmail.com', 'Admin');

				// Add a recipient
				$mail->addAddress($this->input->post('email'));
				// Email subject
				$mail->Subject = 'please Verification your email';
				// Set email format to HTML
				$mail->isHTML(true);

				// Email body content
				$mailContent = "<h1>Dear,</h1>
				<p>This is your email verification in order to activate your account. Best regards</p>" . "<a href=" . base_url() . "ctrl/active/" . base64_encode($code . '' . $email) . ">Click Here To Activate</a>";
				$mail->Body = $mailContent;
				$send = $mail->send();

				if ($send) {
					$this->session->set_flashdata('message', "<div class=\"alert alert-success\" role=\"alert\">
					Berhasil
					</div>");
					redirect('ctrl');
				} else {
					$this->session->set_flashdata('message', '"<div class=\"alert alert-success\" role=\"alert\">
					Gagal
					</div>"');
					redirect('ctrl/register');
				}
			} else {
				$this->session->set_flashdata('message', '<small class="text-danger label-material" role="alert">Please fill the captcha!</small>');
				redirect('ctrl/register');
			}
		}
	}

	public function active($code)
	{
		$data = base64_decode($code);

		$token = $this->user_model->checkUser($data);
		if ($token == "berhasil") {
			$this->session->set_flashdata('message', '<p class="text-success label-material" role="alert">Activation Success</p>');
			redirect('Ctrl');
		} else {
			$this->session->set_flashdata('message', '<small class="text-danger label-material" role="alert">Invalid Email!</small>');
			redirect('Ctrl');
		}
	}
	public function logout()
	{
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('role_id');

		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
				Berhasil Logout !!
				</div>');
		redirect('ctrl/');
	}
	public function showAllData()
	{
		$result = $this->user_model->getAllData();
		echo json_encode($result);
	}

	//Ajax Controller Add Data
	public function addNew()
	{
		$result = $this->user_model->addData();
		$msg['success'] = false;
		$msg['type'] = 'add';
		if ($result) {
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	//Ajax Controller Edit Data
	public function editData()
	{
		$result = $this->user_model->editData();
		echo json_encode($result);
	}

	public function updateData()
	{
		$result = $this->user_model->updateData();
		$msg['success'] = false;
		$msg['type'] = 'update';
		if ($result) {
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	public function deleteData()
	{
		$result = $this->user_model->deleteData();
		$msg['success'] = false;
		if ($result) {
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
}
