<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

	public function index()
	{
		$data['title'] = 'Home';
		$data['log_in'] = $this->db->get_where('log_in', ['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('user/index', $data);
	}

	public function table()
	{
		$this->load->library('user_agent');
		$data['browser'] = $this->agent->browser();
		$data['browser_version'] = $this->agent->version();
		$data['os'] = $this->agent->platform();
		$data['ip_address'] = $this->input->ip_address();
		// $this->load->view('user/table', $data);
		$data['log_in'] = $this->db->get_where('log_in', ['email' => $this->session->userdata('email')])->row_array();
		$data['tampil'] = $this->db->get('log_in');
		$this->load->view('user/table', $data);
	}
}
