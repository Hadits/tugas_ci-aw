<?php
class user_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insert($data)
    {
        // $this->db->insert('log_in', $data);
        // return $this->db->insert_id();

        $sql = "INSERT INTO log_in(`name`,email,`password`,is_active,data_created,code) VALUES(?,?,?,?,NOW(),?)";
        $hasil = $this->db->query($sql, $data);
        return ($hasil);
    }

    public function getUser($name)
    {
        $query = $this->db->get_where('user', array('name' => $name));
        return $query->row_array();
    }

    public function checkUser($data)
    {
        $code = substr($data, 0, 10);
        $email = substr($data, 10, (strlen($data) - 10));

        $sql = "SELECT * FROM log_in WHERE email = ? AND code  = ?";
        $hsl = $this->db->query($sql, array($email, $code));

        if ($hsl->num_rows() > 0) {
            $sql1 = "UPDATE log_in SET is_active = 1 WHERE email = ?";
            $hsl1 = $this->db->query($sql1, array($email));
            return "berhasil";
        } else {
            return "gagal";
        }
    }

    public function changeActive($id)
    {
        $sql = "UPDATE log_in SET is_active = 1 WHERE token = ?";
        $query = $this->db->query($sql, array($id));
        return $query;
    }

    public function checkEmail($id)
    {
        return $this->db->get_where('log_in', $id)->row_array();
    }
    public function userName($name)
    {
        return $this->db->get_where('log_in', $name)->row_array();
    }

    //Model for Ajax Get All Data
    public function getAllData()
    {
        $query = $this->db->get('log_in');
        //Check if Data Exists
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function addData()
    {
        $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = substr(str_shuffle($set), 0, 10);

        $field = [
            'name' => htmlspecialchars($this->input->post('name', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'is_active' => 1,
            'code' => $code
        ];
        $this->db->insert('log_in', $field);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function editData()
    {
        $id = $this->input->get('id');
        $this->db->where('id', $id);
        $query = $this->db->get('log_in');

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function updateData()
    {
        $id = $this->input->post('detectId');
        $field = [
            'name' => htmlspecialchars($this->input->post('name', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
        ];
        $this->db->where('id', $id);
        $this->db->update('log_in', $field);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function deleteData()
    {
        $id = $this->input->get('id');
        $this->db->where('id', $id);
        $this->db->delete('log_in');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
